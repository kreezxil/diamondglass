package com.kreezcraft.diamondglass.block;

import com.kreezcraft.diamondglass.DiamondGlass;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.ToolType;

import javax.tools.Tool;
import java.awt.*;

public class BasicBlock extends Block {

    public BasicBlock(String name, float hard, float resistance) {
        super(Properties.create(Material.SAND).hardnessAndResistance(hard, resistance));
        this.setRegistryName(new ResourceLocation(DiamondGlass.ModId, name));
    }

    public BasicBlock(String name, float both) {
        super(Properties.create(Material.SAND).hardnessAndResistance(both));
        this.setRegistryName(new ResourceLocation(DiamondGlass.ModId, name));
    }

    @Override
    public boolean isToolEffective(BlockState state, ToolType tool) {
        if (tool == ToolType.PICKAXE)
            return true;
        return false;
    }

    @Override
    public int getHarvestLevel(BlockState state) {
        return 3;
    }


    @Override
    public boolean canHarvestBlock(BlockState state, IBlockReader world, BlockPos pos, PlayerEntity player) {
        return true;
    }
}
