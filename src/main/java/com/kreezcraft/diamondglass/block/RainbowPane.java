package com.kreezcraft.diamondglass.block;

import com.kreezcraft.diamondglass.InitBlocks;
import com.kreezcraft.diamondglass.DiamondGlass;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.PaneBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.ToolType;

public class RainbowPane extends PaneBlock {

    public RainbowPane() {
        super(Block.Properties.create(Material.GLASS).hardnessAndResistance(InitBlocks.glassHardness).notSolid().sound(SoundType.GLASS));
        this.setRegistryName(new ResourceLocation(DiamondGlass.ModId, "rainbowglasspane"));
    }

    @Override
    public boolean isToolEffective(BlockState state, ToolType tool) {
        if (tool == ToolType.PICKAXE)
            return true;
        return false;
    }

    @Override
    public int getHarvestLevel(BlockState state) {
        return 3;
    }

    @Override
    public boolean canHarvestBlock(BlockState state, IBlockReader world, BlockPos pos, PlayerEntity player) {
        return true;
    }

}
