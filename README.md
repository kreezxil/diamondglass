[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Description

I wanted a really strong glass but without adding a mod that added lots of other stuff I didn't need.

![](https://i.imgur.com/eleY718.png)

![](https://i.imgur.com/4HEsK3v.png)

![](https://i.imgur.com/pVFRzj3.png)

![](https://i.imgur.com/mEckP4h.png)

![](https://i.imgur.com/PI1gPLm.png)

The glass in this mod is based on diamond and sand. The diamond glass blocks, panes, slabs, and stairs have the blast resistance of obsidian, are hard as heck to remove once placed, and have that special texture where they are both seamless and having no membranes. The remainder of the support blocks are based on sand and are only added because they are a natural progression of the Diamond Sand block.

## Features

*   Config for both server and GUI for client.
*   You can change the hardness and explosion resistance of the two types of blocks provided.
*   The Diamond Glass variant can be set to invulnerable making it ideal for Wither and Ender Dragon Museums!

## Modpacks

Yes you can use my mod in your pack.

## Credit

*   [Shadows\_of\_Fire](https://minecraft.curseforge.com/members/Shadows_Of_Fire) - Thank you for all of the wonderful work you did for me.
*   [Minecraft Mod Development on Discord](https://discord.gg/WbzhX7) - Lot's of very smart and knowledgeable people

## Help a Veteran today

I am Veteran of United States Army. I am not disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [https://patreon.com/kreezxil](https://patreon.com/kreezxil) .

**This project is proudly powered by FORGE**, without whom it would not be possible. Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
